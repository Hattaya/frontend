const StatusService = {
  stateList: [
    {
      _id: 1,
      topic: 'การลงสมัครสหกิจ',
      status: 'ผ่านคุณสมบัติ',
      detail: ''
    },
    {
      _id: 2,
      topic: 'รายละเอียดเอกสารสมัครสหกิจ',
      status: 'เอกสาถูกต้อง',
      detail: ''
    },
    {
      _id: 3,
      topic: 'การสมัครกับสถานประกอบการ',
      status: 'ผ่านคุณสมบัติ',
      detail: 'วันสัมภาษณ์ : 01-01-2020'
    },
    {
      _id: 4,
      topic: 'การสอบสัมภาษณ์',
      status: 'รอสัมภาษณ์',
      detail: ''
    },
    {
      _id: 5,
      topic: 'การไปสหกิจ',
      status: 'รออัปเดต',
      detail: ''
    },
    {
      _id: 6,
      topic: 'การจบสหกิจ',
      status: 'รออัปเดต',
      detail: ''
    }
  ],

  getState () {
    return [...this.stateList]
  }
}

export default StatusService
